# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

Create a function that takes two arguments and returns the minimum value
If the values are the same, either one can be returned

Used min() function, was supposed to use if statements. Will adjust to use the tools that have been taught.

### 04 max_of_three

Create a function that returns the maximum of three values
Return either value if two of the values are the same
Return any of the values, if they are all the same

Did a long way. Solution makes more sense

### 06 can_skydive

Create a function that determines if someone can go skydiving
Person must be >= 18 y/o or Person must hav signed consent form

### 09 is_palindrome

Create a function that test if a string can be read the same forward as it can be backward
The function needs to use built in reversed and join functions

## 10 is_divisible_by_3

Create a function that returns the string "fizz" if divisble by 3
If the number is not divisible by 3, return the value

## 11 is_divisible_by_5

Create a function that returns the string "buzz" if divisble by 5
If the number is not divisible by 5, return the value

## 12 fizzbuzz

Create a function that returns fizzbuzz if argument is divided by 5 and 3 evenly
If the value is only evenly divisible by 3 return fizz
If the value is only evenly divisible by 5 return buzz
Return the number if not evenly divisble by 3 or 5

## 14 can_make_pasta

Create a function that
        Returns True if "flour", "eggs", and "oil" in a list
        Returns False if those ingredients are not in the list
The list will always have 3 ingredients in it

## 16 is_inside_bounds

Create a function that takes two arguments, a x coordinate and a y coordinate
The function needs to test both coordinates to make sure they ar inclusively between 10 and 0

## 19 is_inside_bounds

Create a function th includes six parameters for checking if coordinates are inside of a rectangle
For True, the following must be true:
- x >= rect_x
- y >= rect y
- x <= (rect_x + rect_width)
- y <= (rect_y + rect height)

Else:
    False

## 20 has_quorum

Create a function that checks if:
- attendees_list >= (.5 * members_list)

## 22 gear_for_day

Create a function that returns a list of gear needed for the day:
- If day != sunny and day == workday add "Umbrella" to list
- If day == workday add "Laptop" to list
- If day != workday add "surfboard" to list


## calculate_average

Create a function which takes a list of numerical values and return the average
- If list is empty, rturn None

## 25 calculate_sum

Create a function that takes a list of numerical values and returns the sum
- if list is empty the function should return None


## 26 calculate_grade

Create a function that returns the letter of a students grade depending on the average of their scores based on a list of numerical values
- A if avg >= 90
- B if avg >= 80 and < 90
- C if avg >= 70 and < 80
- D if avg >= 60 and < 70
- Else F

## 27 max_in_list

Create a function that returns the highest number in a list
- If there are no values return None

## 28 remove_duplicate_letters

Create a function that takes a string and removes duplicate letters

## 30 find_second_largest

Create a list that finds the second largest number in a list of numerical values
- If the list is empty, return None
- If the list only has one value return None

## 31 sum_of_squares

Create a function that takes a list of numerical values and returns the sum of each item squared

## 32 sum_of_first_n_numbers

Create a function that takes a numerical limit and return the sum of the numbers from 0 to the numerical limit
- If the value is less than 0, then return None

## 33 sum_of_first_n_even_numbers

Create a function that takes a numerical count n and returns the sum of the first n even numbers

## 34 count_letters_and_digits

Create a function that takes a string of letters and numbers and returns the count of letters and numbers

## 35 count_letter_and_digits

Create a function that takes a string of letters and numbers and returns the count of letters and numbers

## 37 pad_left

Create a function that turns a number into a string by adding padding to the left of it

## 39 reverse_dictionary

Create a function that takes a dictionary and reverses the key value pairs inside of a new dictionary

## 41 add_csv_lines

Create a function which takes a list and returns a new list that contains the sum of the comma seperated values

## 42 pairwise_add

Create a function which takes 2 lists of the same size and returns a new list with the sum of the indexed values of each list

## 43 find_indexes

Create a function that takes 2 parameters, a list and a search term, and returns a new list with the indexes of the search term

# 44 translate

Create a function which takes two parameters, a list of keys and a dictionary, and returns a new list containing to the values of the keys in the dictionary. If key dopes not exist, it should say None.

## 46 make_sentences

Create a function that takes a list of subject, a list of verbs, and a list of objects, then returns a list of three word sentences

## 47 check_password

Create a function which checks a password and ensures it is valid

##  48 count_word_freq

Create a function that takes a string and returns a dictionary that counts the frequency of each word

## 49 sum_two_numbers

Create a funtion that takes two numbers and returns the sum of both

## 50 halve_the_list

Create a function that takes a list and returns two lists containing half of the original list

## 51 safe_divide

Create a function that takes a numerator and a denomintor then returns the quotient


## 52 generate_lottery_numbers

Create a function that returns a list of six numbers between 1-40
- no duplicates

## 53 username_from_email

Create a function that takes an email and returns the username

## 54 check_input

Create a function that takes any number between 1 and 10 then returns the corresponding roman numeral

## 56 num_concat

Create a function that takes two numerical values and returns the concatenated string of the values

## 57 sum_fraction_sequence

Create a function that takes a numberand returns the sum of the fractions of the form

## 58 group_cities_by_state

Create a function that takes a list of cities and states and returns a dictionary

## 59 specific_random

Create a function that returns a random number from a list of numbers divisible by 5 and 7

## 60 only_odds

Create a function that take a list of numbers and returns only the odd numbers from that list

## 61 remove_duplicates

Create a function that takes a list and returns a list with duplicates removed

## 62 basic_calculator

Create a function that takes two numbers and an operator and return the calculation

## 63 shift_letters

Create a function that shifts each character in a string to the next letter in the alphabet

## 64 temperature_differences

Create a function that takes a list of high temps and a list of low temps and return the a list with differences

## 65 biggest gap

Create a function that takes a list of numbers and returns the largest gap between each numbers

## 66 class Book
Write a class named book
- State:
    - author name (string)
    - title (string)

- Behavior
    - get_author, returns "Author: {author name}
    - get title, returns "Title: {title}


## 67 class Employee

Write a class named Employee
- State:
    - first_name (string)
    - last_name (string)

- Behavior
    - get_fullname, returns "first_name last_name"
    - get_email, returns "first_name.last_name@company.com"

## 68 class Person
- State
    - name (string)
    - hated_food_list (list)
    - loved_food_list (list)

- Behavior
    - taste, returns:
        - None, if food name is not in hated or loved lists
        - True, if food name is on Loved list
        - False if food name is on hated list

## 69 class Student
- State
    - name (string)

- Behavior
    - add_score(self, score) (list)
    - get_average()

## 70 class Book
Write a class named book
- State:
    - author name (string)
    - title (string)

- Behavior
    - get_author, returns "Author: {author name}
    - get title, returns "Title: {title}

## 71 class Employee

Write a class named Employee
- State:
    - first_name (string)
    - last_name (string)

- Behavior
    - get_fullname, returns "first_name last_name"
    - get_email, returns "first_name.last_name@company.com"

## 72 class Person
- State
    - name (string)
    - hated_food_list (list)
    - loved_food_list (list)

- Behavior
    - taste, returns:
        - None, if food name is not in hated or loved lists
        - True, if food name is on Loved list
        - False if food name is on hated list

## 73 class Student
- State
    - name (string)

- Behavior
    - add_score(self, score) (list)
    - get_average()

## 74 BankAccount
- State
    - balance

- Behavior
    - get_balance(), returns the bank account's current balance
    - deposit(amount), returns the amount deposited into bank account
    - withdraw(amount), returns the amount withdrawn from the bank account

## 75 BankAccount
- State
    - balance

- Behavior
    - get_balance(), returns the bank account's current balance
    - deposit(amount), returns the amount deposited into bank account
    - withdraw(amount), returns the amount withdrawn from the bank account

## 76 Fix wiithdraw behavior in class BankAccount

Fix the withdraw behavior so that it shows a value error if the user tries to withdraw more than what is in their account

## 77 Circle
- State
    - radius, a nonnegative value

- Behavior
    - calculate_perimeter(), returns the length of the perimeter of a circle
    - calculate_area(), returns the area of a circle

## 78 Circle
- State
    - radius, a nonnegative value

- Behavior
    - calculate_perimeter(), returns the length of the perimeter of a circle
    - calculate_area(), returns the area of a circle

## 79 ReceiptItem

- State
    - quantity
    - price

- Behavior
    - get_total()


## 80 Receipt
- State
    - tax_rate

- Behavior
    - add_item(item)
    - get_subtotal()
    - get_total()

## 81 Animal

Create a class Animal
- State:
    - number_of_legs
    - primary_color

- Behavior
    - describe()

Then create three new dog, cat, and snake classes that inherit from Animal
